from django import forms
from .models import OrderInfo, RepairsType
from django.contrib.auth.models import User


class OrderInfoForm(forms.ModelForm):

    order_type = forms.ModelMultipleChoiceField(queryset=RepairsType.objects.all(),
                                                widget=forms.CheckboxSelectMultiple,
                                                label='Выберите вид работ')

    description = forms.CharField(max_length=500, label="Опишите детали заказа")

    class Meta:

        model = OrderInfo
        fields = ('order_type', 'description', )


class RegisterForm(forms.Form):

    username = forms.CharField(label='выберите логин')
    email = forms.EmailField()
    password = forms.CharField()
    phone = forms.IntegerField()

    def clean_username(self):
        """
        Проверим уникальность юзернейма
        """
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username).count() > 0:
            raise forms.ValidationError(_('User %s is already exists') % username)
        return username

    def clean(self):
        """
        Подготовка чистых данных для вставки в модель
        """
        data = super(RegisterForm, self).clean()

        data['user'] = User.objects.create_user(
            username=data['username'],
            password=data['password'],
            email=data['email'],
            phone=data['phone'],
        )
        return data
