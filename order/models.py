from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User


class UserProfile(models.Model):

    user = AutoOneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    order_info = models.ForeignKey('OrderInfo', null=True, blank=True, on_delete=models.SET_NULL)
    phone = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Master(models.Model):
    master_name = models.CharField(max_length=200, db_index=True)
    master_category = models.ManyToManyField('RepairsType', null=True, blank=True)
    master_description = models.CharField(max_length=500, null=True, blank=True)
    registration_date = models.DateField()
    is_available = models.BooleanField(default=True, )

    def get_category(self):
        category_list = self.master_category.get_queryset()
        category_str = ''
        for categories in category_list:
            category_str += ', ' + categories.description
        return category_str.lstrip(', ')
    get_category.short_description = 'Специализация'
    # master_photo = models.ImageField(upload_to='media/%Y/%m/%d/', blank=True, null=True)

    def __str__(self):
        return self.master_name


class OrderInfo (models.Model):

    created_date = models.DateTimeField(default=timezone.now)
    who_create = models.ForeignKey(UserProfile)
    order_type = models.CharField(max_length=500, null=True, verbose_name='Тип ремонта')
    description = models.CharField(max_length=500, null=True, blank=True, verbose_name='Коментарий к заказу')
    quantity = models.CharField(max_length=100, null=True, blank=True)
    who_perform = models.ForeignKey(Master, null=True, blank=True)
    is_completed = models.BooleanField(default=False)

    def __str__(self):
        return self.description


class RepairsType(models.Model):

    description = models.CharField(max_length=500, db_index=True)

    masters = models.ManyToManyField(Master)

    def get_masters(self):
        master_list = self.masters.get_queryset()
        masters_str = ''
        for masters in master_list:
            masters_str += ', ' + masters.master_name
        return masters_str.lstrip(', ')
    get_masters.short_description = 'Мастера'

    def __str__(self):
        return self.description


