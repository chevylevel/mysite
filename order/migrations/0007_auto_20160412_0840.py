# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-12 04:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_master_master_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='master',
            name='master_category',
        ),
        migrations.AddField(
            model_name='master',
            name='master_category',
            field=models.ManyToManyField(blank=True, null=True, to='order.MasterCategory'),
        ),
    ]
