from django.conf.urls import url
from order import views

app_name = 'order'
urlpatterns = [
    url(r'^$', views.OrderInfoView.as_view(), name='make_order'),
]
