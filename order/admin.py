from django.contrib import admin
from order.models import RepairsType, UserProfile, Master, OrderInfo
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


class RepairsTypeAdmin(admin.ModelAdmin):

    list_display = ('description', 'get_masters')

admin.site.register(RepairsType, RepairsTypeAdmin)


class MasterAdmin(admin.ModelAdmin):

    list_display = ('master_name', 'master_description', 'get_category', 'is_available', )

admin.site.register(Master, MasterAdmin)


class OrderInfoAdmin(admin.ModelAdmin):

    list_display = ('id', 'who_create', 'description', 'is_completed')

admin.site.register(OrderInfo, OrderInfoAdmin)


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Заказчик'


class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
