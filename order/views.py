from django.shortcuts import render
from .models import RepairsType, Master, OrderInfo
from .forms import OrderInfoForm, RegisterForm
from django.utils import timezone
from django.shortcuts import redirect
from django.views.generic.edit import FormView
from django.views.generic.base import View
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect

from functools import wraps
from django.utils.decorators import classonlymethod
from django.contrib.auth.decorators import login_required


def view_decorator(fdec, subclass=False):

    def decorator(cls):
        if not hasattr(cls, "as_view"):
            raise TypeError("You should only decorate subclasses of View, not mixins.")
        if subclass:
            cls = type("%sWithDecorator(%s)" % (cls.__name__, fdec.__name__), (cls,), {})
        original = cls.as_view.__func__

        @wraps(original)
        def as_view(current, **initkwargs):
            return fdec(original(current, **initkwargs))
        cls.as_view = classonlymethod(as_view)
        return cls
    return decorator

my_login_required = view_decorator(login_required(login_url='/login/'), subclass=False)


class RegisterFormView(FormView):

    ''' form_class = UserCreationForm
    success_url = '/login/'
    template_name = 'order/register.html'

    def form_valid(self, form):

        form.save()
        return super(RegisterFormView, self).form_valid(form)'''

    form = RegisterForm()
    if form.is_valid():
        data = form.cleaned_data
        profile, created = UserProfile.objects.get_or_create(
            user = data['user'],  # user - модель, а не строка
            team = data['team'],  # team тоже модель
        )


class LoginFormView(FormView):

    form_class = AuthenticationForm
    template_name = "order/login.html"
    success_url = '/'

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


def index(request):

    return render(request, 'order/index.html', {})


@my_login_required
class OrderInfoView(FormView):

    form_class = OrderInfoForm
    template_name = 'order/make_order.html'
    success_url = '/your_order/'

    def form_valid(self, form):
        order = form.save(commit=False)
        order.who_create = self.request.user
        order.save()

        return super(OrderView, self).form_valid(form)


def your_order(request, order_id):

    order = OrderInfo.objects.get(id=order_id)

    return render(request, 'order/your_order.html', {'order': order})
